Nova.booting((Vue, router) => {
    Vue.component('index-nova-ckeditor-translatable', require('./components/IndexField'));
    Vue.component('detail-nova-ckeditor-translatable', require('./components/DetailField'));
    Vue.component('form-nova-ckeditor-translatable', require('./components/FormField'));
})
